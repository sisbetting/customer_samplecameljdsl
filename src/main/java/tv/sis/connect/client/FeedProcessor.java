package tv.sis.connect.client;

import org.springframework.stereotype.Component;

/**
 * Example Camel Java DSL Router to consume the SIS Connect feed with separate processing for
 * events related to dog and horse racing
 */
@Component
public class FeedProcessor extends FeedRouteBuilder {

    /**
     * Configure the Camel routing rules using Java code...
     */
    @Override
    public void configureRoutes() throws Exception {

        from("sisjms:{{sis-q-connection.queue-name}}")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:MessageProcessor?method=process${header.entityType}")
                .log("Message for ${header.entityType} entity");

        /* CODE BELOW: reflects the use of selectors on a queue.

        from("sisjms:{{sis-q-connection.queue-name}}?selector=categoryCode='DR'")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:dogRaceMessageProcessor?method=process${header.entityType}")
                .log("Processed dog racing message for ${header.entityType} entity");

        from("sisjms:{{sis-q-connection.queue-name}}?selector=categoryCode='HR'")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:horseRaceMessageProcessor?method=process${header.entityType}")
                .log("Processed horse racing message for ${header.entityType} entity");

         */

    }

}
