package tv.sis.connect.client;

/**
 * Exception type to signal a poison message. Wrap any other type of exception in it
 */
public class BadMessageException extends Exception {
    public BadMessageException(Throwable throwable) {
        super(throwable);
    }

    public BadMessageException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public BadMessageException(String s) {
        super(s);
    }
}
