package tv.sis.connect.client;

/**
 * Dummy domain class just for illustration - the transformer bean would create from XML/JSON
 */
public class Meeting {
    private String description;

    public Meeting(String message) {
        this.description = message;
    }

    @Override
    public String toString() {
        return description;
    }
}
