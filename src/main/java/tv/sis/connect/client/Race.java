package tv.sis.connect.client;

/**
 * Dummy domain class just for illustration - the transformer bean would create from XML/JSON
 */
public class Race {
    private String description;

    public Race(String message) {
        this.description = message;
    }

    @Override
    public String toString() {
        return description;
    }
}
