package tv.sis.connect.client;

import org.springframework.stereotype.Component;

/**
 * Example just for illustration. This would use e.g. JAXB or GSON to transform the payload
 * to an object
 */
@Component
public class RaceMessageTransformer {

    public Race extractRace(String message) throws BadMessageException {
        // Real world code should throw a BadMessageException if unable to parse/transform the String to the target object:
        // This will result in the message going to the dead letter channel and unblocking the queue, e.g.
        // try { // parse XML } catch (XMLParseException e) { throw new BadMessageException(e); }
        return new Race(message);
    }

    public Meeting extractMeeting(String message) throws BadMessageException {
        return new Meeting(message);
    }

}
