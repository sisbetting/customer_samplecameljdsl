package tv.sis.connect.client;

import org.apache.camel.component.jms.JmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.connection.CachingConnectionFactory;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

/**
 * All configuration is managed in this class and applicationContext.xml - runtime properties and Spring context
 */
@Configuration
@EnableJms
@ComponentScan
@ConfigurationProperties("sis-q-connection")
@ImportResource({"classpath*:spring-context.xml"})
public class AppConfig {

    // Vendor-specific JMS Provider is configured externally in XML
    @Autowired
    private ConnectionFactory jmsConnectionFactory;

    /**
     * Should always use a CachingConnectionFactory with Spring JMS unless running in a Java EE container and looking
     * up a container-managed pooled connection factory using JNDI
     */
    @Bean
    public ConnectionFactory connectionFactory() throws JMSException {
        return new CachingConnectionFactory(jmsConnectionFactory);
    }

    /**
     * Component for sisjms: URIs
     */
    @Bean
    public JmsComponent sisjms(ConnectionFactory connectionFactory) {
        // Note the default listener starts a single consumer by default, maintaining message ordering
        JmsComponent result = new JmsComponent();
        result.setConnectionFactory(connectionFactory);
        // Use transactions (roll back for redelivery if a processor fails downstream)
        result.setTransacted(true);
        return result;
    }

}
