package tv.sis.connect.client;

import org.apache.camel.CamelExchangeException;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;

/**
 * Base class for Feed routes providing necessary exception handling
 */
public abstract class FeedRouteBuilder extends RouteBuilder {
    @Value("${camel.tracing}")
    private boolean camelTracing;
    @Value("${dead-letter.log-handled}")
    private boolean logDlcHandled;

    public final void configure() throws Exception {
        getContext().setTracing(camelTracing);

        // Set up dead letter channel for poison messages as default handler
        errorHandler(deadLetterChannel("{{dead-letter.uri}}").logHandled(logDlcHandled));
        onException(CamelExchangeException.class, BadMessageException.class) // Something wrong with message or headers
                .maximumRedeliveries(0); // Send to DLC
        onException(Throwable.class) // Anything else should be recoverable - Retry indefinitely with backoff
                .maximumRedeliveries(-1).useExponentialBackOff().handled(true);

        configureRoutes();
    }

    public abstract void configureRoutes() throws Exception;

}
