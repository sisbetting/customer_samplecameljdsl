package tv.sis.connect.client.processor;

import tv.sis.connect.client.Meeting;
import tv.sis.connect.client.Race;

/**
 * Created by mgreenwood on 31/07/2017.
 */
public class MessageProcessor {

    private volatile int raceCount, meetingCount, processedCount;

    public void processRace(Race race) throws Exception {
        // Perform required processing for horse race
        raceCount++;
        processedCount++;
    }

    public void processMeeting(Meeting meeting) throws Exception {
        // Perform required processing for horse meeting
        meetingCount++;
        processedCount++;
    }
}
