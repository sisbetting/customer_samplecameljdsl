package tv.sis.connect.client.processor;

import org.springframework.stereotype.Component;
import tv.sis.connect.client.Meeting;
import tv.sis.connect.client.Race;

/**
 * Example Bean to process Horse race related event messages
 */
@Component
public class HorseRaceMessageProcessor {

    public void processRace(Race race) throws Exception {
        // Perform required processing for horse race

    }

    public void processMeeting(Meeting meeting) throws Exception {
        // Perform required processing for horse meeting

    }

}
