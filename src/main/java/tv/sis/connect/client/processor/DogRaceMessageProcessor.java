package tv.sis.connect.client.processor;

import org.springframework.stereotype.Component;
import tv.sis.connect.client.Meeting;
import tv.sis.connect.client.Race;

/**
 * Example Bean to process Dog Race related event messages
 */
@Component
public class DogRaceMessageProcessor {

    private volatile int raceCount, meetingCount;

    public void processRace(Race race) throws Exception {
        // Perform required processing for dog race

        raceCount++;
    }

    public void processMeeting(Meeting meeting) throws Exception {
        // Perform required processing for dog meeting

        meetingCount++;
    }

}
