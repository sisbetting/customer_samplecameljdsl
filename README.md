Example Camel Java DSL Client for SIS Connect Feed
==================================================

**ALL SAMPLE CODE IS PROVIDED ON AN "AS-IS" BASIS FOR GENERAL GUIDANCE, WITH AN EXISTING UNDERSTANDING OF JAVA, CAMEL AND SPRING APIs AND CONVENTIONS BEING PRESUMED. SIS CANNOT PROVIDE GENERAL 
SUPPORT FOR THE USE OF JAVA, OR THE CAMEL AND SPRING FRAMEWORKS, WHICH ARE DOCUMENTED AND DISCUSSED EXTENSIVELY ONLINE. PLEASE SEE https://bitbucket.org/sisbetting/customer_sampleclient 
FOR ADDITIONAL SAMPLES USING OTHER LANGUAGES AND/OR FRAMEWORKS**

To build and run this project standalone use

    mvn install
    java  -jar target/CamelDSLClient-1.0.jar

To run this project during development use

    mvn spring-boot:run

To compile code changes and run the standalone tests during development use

    mvn test

To build as a WAR (e.g. to deploy to a Java EE application server), see http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#build-tool-plugins-maven-packaging


**Prerequisites**

Open/Oracle/IBM Java 8 or later recommended. May work with Java 6/7 - compatible at the code level but not tested.

Oracle Java (only) requires the JCE Unlimited Strength Policy Files to be installed from http://bit.ly/1beztkB otherwise SSL negotiation with our servers will fail.

Maven - version 3.0 or later. Download from http://maven.apache.org

Maven will manage most external dependencies however the JMS provider library is not available from a public Maven repository.


**Configuration**

Queue connection parameters are set via application.yml. See the Spring documentation for how to set
this up with different profiles for different environments: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html

The JMS client connection is configured in AppConfig.connectionFactory(). If deploying to a Java EE
application server you will probably prefer to have it manage JMS connections and change this method to
lookup a JNDI resource instead.

Check with your Account Manager or SIS Technical Contact on how to obtain connection credentials and client certificates.


**Consuming the Feed**

The feed can be consumed by a single process, or split for multiple consumers using message selectors.

The key to this is the FeedProcessor class, which routes incoming messages to processor objects. In the 
simplest case, to have all messages consumed by one processor, configure as follows:

```java
/**
 * Example Camel Java DSL Router to consume the SIS Connect feed with common processing for all events
 */
@Component
public class FeedProcessor extends FeedRouteBuilder {

    /**
     * Configure the Camel routing rules using Java code...
     */
    @Override
    public void configureRoutes() throws Exception {

        from("sisjms:{{sis-q-connection.queue}}")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:raceMessageProcessor?method=process${header.entityType}")
                .log("Processed racing feed message for ${header.entityType} entity");
    }

}

```
The above example requires a RaceMessageProcessor component to be available in the application context. This can be
achieved simply by renaming DogRaceMessageProcessor in the sample code and deleting HorseRaceMessageProcessor.

If it would be helpful to split the feed, e.g. to separate receipt of messages for dog and horse racing, this can be achieved by message selectors. 

See the SIS Connect Wiki for available message properties that can be used to split feed processing: https://siswiki.sis.tv:8090/display/SC/High+Level+Overview

For example, to process horse racing, BAGS and non-BAGS dog racing independently, would be achieved like this:
*Please note, the subCode property is not available yet. It's there just to show how the feed can be split
and processed separately.*

```java
/**
 * Example Camel Java DSL Router to consume the SIS Connect feed with separate processing for
 * events related to dog and horse racing
 */
@Component
public class FeedProcessor extends FeedRouteBuilder {

    /**
     * Configure the Camel routing rules using Java code...
     */
    @Override
    public void configureRoutes() throws Exception {

        from("sisjms:{{sis-q-connection.queue}}?selector=categoryCode='HR'")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:horseRaceMessageProcessor?method=process${header.entityType}")
                .log("Processed horse racing message for ${header.entityType} entity");
        
        from("sisjms:{{sis-q-connection.queue}}?selector=categoryCode='DG' and subCode='BG'")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:bagsRaceMessageProcessor?method=process${header.entityType}")
                .log("Processed BAGS racing message for ${header.entityType} entity");

        from("sisjms:{{sis-q-connection.queue}}?selector=categoryCode='DG' and subCode='NB'")
                .toD("bean:raceMessageTransformer?method=extract${header.entityType}")
                .toD("bean:nonBagsRaceMessageProcessor?method=process${header.entityType}")
                .log("Processed non-BAGS racing message for ${header.entityType} entity");

    }

}
```
**Error Handling**

Basic error handling is included in the sample code, for:

- "Poison messages" - messages that are invalid and could therefore never be processed - which are diverted to a Dead Letter Log (endpoint 
configurable via logback.xml). While poison messages should never be encountered in production they need to be accommodated as a possibility, to prevent the queue becoming blocked
 
- Unhandled exceptions, resulting in transactional redelivery (assuming that there is some downstream problem with e.g. a web service or a database), logging an error - i.e. to be picked up by an external monitor - and delaying redelivery with exponential backoff